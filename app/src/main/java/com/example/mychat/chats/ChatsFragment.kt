package com.example.mychat.chats

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.mychat.R
import com.example.mychat.models.Chat
import com.example.mychat.models.User
import com.example.mychat.users.UsersAdapter
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_chats.view.*

class ChatsFragment : Fragment() {

    private val chatsViewModel by lazy { ViewModelProviders.of(this).get(ChatsViewModel::class.java) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chats, container, false)

        chatsViewModel.loadUsersChats()

        view.apply {
            recycler_view_users_with_chat.setHasFixedSize(true)
            recycler_view_users_with_chat.layoutManager= LinearLayoutManager(activity)
            val users= arrayListOf<Chat>()
            val chatsAdapter= ChatsAdapter(context, users)
            recycler_view_users_with_chat.adapter=chatsAdapter

            activity?.let {
                chatsViewModel.usersAndLastChat.observe(it, Observer {users->
                    users?.let{
                        chatsAdapter.refreshchats(users)
                    }
                })
            }
        }

        FirebaseInstanceId.getInstance().token?.let { chatsViewModel.updateToken(it) }

        return view
    }

}
