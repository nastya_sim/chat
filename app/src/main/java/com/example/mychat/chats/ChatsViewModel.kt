package com.example.mychat.chats

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mychat.models.*
import com.example.mychat.notifications.Token

class ChatsViewModel : ViewModel(){
    val messageModel=MessageModel()
    val tokenModel=TokenModel()
    val usersWithChats=MutableLiveData<ArrayList<User>>()
    val currentUserId= UserModel().auth.currentUser?.uid
    val usersAndLastChat=MutableLiveData<ArrayList<Chat>>()

    private fun onCompleteLoadUsersWithChats(users: ArrayList<User>){
        usersWithChats.value=users
        loadUsersAndLastChat()
    }

    fun loadUsersChats(){
        messageModel.loadUsersChats(this::onCompleteLoadUsersWithChats){}
    }

    private fun onCompleteLoadLastChat(lastChat: Chat){
        val oldUserAndLastChat = usersAndLastChat.value
        val userAndLastChat = arrayListOf<Chat>()
        if(usersAndLastChat.value!=null) {
//            var valueForChange = Triple(User(), "", String())
            if (oldUserAndLastChat != null) {
                for (value: Chat in oldUserAndLastChat) {
                    if (value.user.id != lastChat.user.id) {
                        userAndLastChat.add(value)
                    }
                }
            }
//            if (valueForChange.second!="") {
//                usersAndLastChat.value!!.remove(valueForChange)
//            }
        }
//        } else {
//            usersAndLastChat.value=arrayListOf<Triple<User, String, String>>()
//        }
        userAndLastChat.add(lastChat)
        usersAndLastChat.value=userAndLastChat
    }

    private fun loadUsersAndLastChat(){
        usersWithChats.value?.forEach {
            messageModel.loadLastChatMessage(it, this::onCompleteLoadLastChat){}
        }
    }

    fun updateToken(token: String){
        tokenModel.updateToken(token)
    }
}