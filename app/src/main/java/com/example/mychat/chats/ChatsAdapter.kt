package com.example.mychat.chats

import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mychat.R
import com.example.mychat.message.MessageActivity
import com.example.mychat.models.Chat
import de.hdodenhof.circleimageview.CircleImageView


class ChatsAdapter(val context: Context, var chats: List<Chat>) :
    RecyclerView.Adapter<ChatsAdapter.ChatsViewHolder>() {

    class ChatsViewHolder(var context: Context, private val view: View) :
        RecyclerView.ViewHolder(view) {

        val chatname = itemView.findViewById<TextView>(R.id.username)
        val profileImage = itemView.findViewById<ImageView>(R.id.profile_image)
        val statusOnline = itemView.findViewById<CircleImageView>(R.id.img_on)
        val statusOffline = itemView.findViewById<CircleImageView>(R.id.img_off)
        val lastMessage = itemView.findViewById<TextView>(R.id.last_msg)
        val messageNew = itemView.findViewById<ImageView>(R.id.new_mes)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatsViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return ChatsAdapter.ChatsViewHolder(context, view)
    }

    override fun getItemCount(): Int {
        return chats.size
    }

    override fun onBindViewHolder(holder: ChatsViewHolder, position: Int) {
        val chat = chats[position]
        holder.apply {
            chatname.text = chat.user.username

            if (chat.user.imageUrl == "default") {
                profileImage.setImageResource(R.mipmap.ic_launcher_round)
            } else {
                Glide.with(context).load(chat.user.imageUrl).into(profileImage)
            }

            if (chat.user.statusOnline) {
                statusOnline.visibility = View.VISIBLE
                statusOffline.visibility = View.GONE
            } else {
                statusOffline.visibility = View.VISIBLE
                statusOnline.visibility = View.GONE
            }

            messageNew.visibility=View.GONE

            if (chat.sender == chat.user.id) {
                lastMessage.text =  "me: " + chat.message
            } else {
                lastMessage.text =chat.message
                if(chat.messageIsSeen=="false"){
                    messageNew.visibility=View.VISIBLE
                    val notification: Uri =
                        RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                    val ring = RingtoneManager.getRingtone(context, notification)
                    ring.play()
                }
            }

            chatname.setOnClickListener {
                startChatWithchat(chat.user.id)
            }

            profileImage.setOnClickListener {
                startChatWithchat(chat.user.id)
            }
        }
    }

    fun startChatWithchat(userId: String) {
        val intent = Intent(context, MessageActivity::class.java)
        intent.putExtra("userId", userId)
        context.startActivity(intent)
    }

    fun refreshchats(chats: List<Chat>) {
        this.chats = chats
        notifyDataSetChanged()
    }

}
