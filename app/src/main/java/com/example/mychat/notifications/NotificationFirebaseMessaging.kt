package com.example.mychat.notifications

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.example.mychat.message.MessageActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class NotificationFirebaseMessaging(): FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)


        val sented = remoteMessage?.data?.get("sented")
        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null) {
            //if((firebaseUser!=null) and (sented == firebaseUser.uid)){
                sendNotification(remoteMessage!!)
            //}

        }
    }

    private fun sendNotification(remoteMessage: RemoteMessage){
        val user = remoteMessage.data["user"]
        val icon = remoteMessage.data["icon"]
        val title=remoteMessage.data["title"]
        val body = remoteMessage.data["body"]

        val notification: RemoteMessage.Notification? = remoteMessage.notification
        val j = (user!!.replace("[\\D]".toRegex(), ""))?.toInt()
        val intent = Intent(this, MessageActivity::class.java)
        val bundle=Bundle()
        bundle.putString("userId", user)
        intent.putExtras(bundle)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent= j?.let {
            PendingIntent.getActivity(this,
                it, intent, PendingIntent.FLAG_ONE_SHOT)
        }
        val defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder: NotificationCompat.Builder? = icon?.toInt()?.let {
            NotificationCompat.Builder(this)
                .setSmallIcon(it)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentIntent(pendingIntent)
        }
        val noti = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        var i=0
        if (j != null) {
            if(j>0){
                i=j
            }
        }

        if (builder != null) {
            noti.notify(i, builder.build())
        }
    }
}