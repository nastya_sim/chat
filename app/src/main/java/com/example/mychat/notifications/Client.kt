package com.example.mychat.notifications

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Client {
    var retrofit: Retrofit? = null

    constructor()

    fun getClient(url: String): Retrofit {
        if (retrofit == null) {
            retrofit =
                Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create())
                    .build()

        }
        return retrofit!!
    }
}