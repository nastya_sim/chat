package com.example.mychat.notifications

import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService

class NotificatesFirebaseService : FirebaseMessagingService() {


    private fun updateToken(refreshToken: String) {
        var firebaseUser = FirebaseAuth.getInstance().currentUser

        var ref = FirebaseDatabase.getInstance().getReference("Tokens")
        var token = Token(refreshToken)
        if (firebaseUser != null) {
            ref.child((firebaseUser.uid)).setValue(token)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        var firebaseUser = FirebaseAuth.getInstance().currentUser

        var refreshToken = FirebaseInstanceId.getInstance().token
        if (firebaseUser != null) updateToken(refreshToken!!)

    }
}