package com.example.mychat.notifications

class Sender {
    var data = Data()
    var receiver = String()

    constructor(data: Data, receiver: String){
        this.data=data
        this.receiver=receiver
    }
}