package com.example.mychat.notifications

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface APIService {
    @Headers(
            "Content-Type:application/json",
            "Authorization:key=AAAAh20OOxw:APA91bFyxMicnvHjgefDPHKeOr3LvsEJko-ISQHDjTRH0KH28I9h3hb86-dN8hWhXK9Vtz8vIG3dzYteiQm7MyPrtrDcYI4ByjcN_aPtCXVCcR4MfyQqSECteF_Qhb6qasgB9MshfPjy"

    )

    @POST("fcm/send")
    fun sendNotification(@Body body: Sender): Call<NotificationResponse>
}