package com.example.mychat.userprofile

import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.mychat.R

import kotlinx.android.synthetic.main.fragment_user_profile.view.*


class UserProfileFragment : Fragment() {

    private val userProfileViewModel by lazy {
        ViewModelProviders.of(this).get(UserProfileViewModel::class.java)
    }

    //private val authViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }

    private val IMAGE_REQUEST = 1
    private lateinit var imageUri: Uri

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user_profile, container, false)
        userProfileViewModel.loadDataOfCurrentUser()

        activity?.let {
            userProfileViewModel.dataOfCurrentUser.observe(it, Observer { user ->
                view.apply {
                    username.text = user.username
                    if (user.imageUrl == "default") {
                        profile_image.setImageResource(R.mipmap.ic_launcher_round)
                    } else {
                        Glide.with(context).load(user.imageUrl).into(profile_image)
                    }

                profile_image.setOnClickListener {
                    openImage()
                }

                    userProfileViewModel.isSuccessLoadData.observe(activity!!, Observer {
                        if(it){
                            view?.progressBar2?.visibility=View.GONE
                        }
                    })
                }
//
            })
        }

        return view
    }

    private fun openImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, IMAGE_REQUEST)
    }

//    private fun getFileExtension(uri: Uri): String? { //опр тип файла
//        val counterResolver = context?.contentResolver
//        val mimeTypeMap = MimeTypeMap.getSingleton()
//        return mimeTypeMap.getExtensionFromMimeType(counterResolver.getType(uri))
//    }

    private fun uploadImage(){
        view?.progressBar2?.visibility=View.VISIBLE

        if(imageUri!=null){
            context?.let { userProfileViewModel.uploadImage(it, imageUri) }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {
            if((requestCode==IMAGE_REQUEST) and (resultCode == RESULT_OK) and (data!=null) and (data.data!=null)){
                imageUri= data.data!!

                uploadImage()
            }
        }
    }

}
