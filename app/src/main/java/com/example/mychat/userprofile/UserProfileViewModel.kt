package com.example.mychat.userprofile

import android.content.Context
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mychat.models.ImageStorage
import com.example.mychat.models.User
import com.example.mychat.models.UserModel

class UserProfileViewModel : ViewModel(){
    private val userModel=UserModel()
    val dataOfCurrentUser=MutableLiveData<User>()
    val isSuccessLoadData=MutableLiveData<Boolean>()

    fun loadDataOfCurrentUser(){
        userModel.loadDataOfCurrentUser({
            dataOfCurrentUser.value=it
        }){}
    }

    private fun onCompleteLoadDataOfUser(isSuccess: Boolean){
        isSuccessLoadData.value=isSuccess
    }

    fun uploadImage(context: Context, imageUri: Uri){
        isSuccessLoadData.value=false
        val imageStorage = ImageStorage(context)
        imageStorage.uploadImage(imageUri, this::onCompleteLoadDataOfUser){}
    }
}