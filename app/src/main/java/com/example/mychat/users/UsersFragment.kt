package com.example.mychat.users

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.mychat.R
import com.example.mychat.auth.AuthViewModel
import com.example.mychat.models.User
import kotlinx.android.synthetic.main.fragment_users.view.*

class UsersFragment : Fragment() {

    private val usersViewModel by lazy { ViewModelProviders.of(this).get(UsersViewModel::class.java) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_users, container, false)

        usersViewModel.loadUsers()

        view.apply {
            recycler_view_allusers.setHasFixedSize(true)
            recycler_view_allusers.layoutManager=LinearLayoutManager(activity)
            val users= arrayListOf<User>()
            val usersAdapter=UsersAdapter(context, users)
            recycler_view_allusers.adapter=usersAdapter

            activity?.let {
                usersViewModel.users.observe(it, Observer {users->
                    users?.let{
                        usersAdapter.refreshUsers(users)
                    }
                })
            }

            search_users.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    val a=0
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    val a=0
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    usersViewModel.searchUsers(s.toString())
                }

            })
        }


        return view
    }


}
