package com.example.mychat.users

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mychat.R
import com.example.mychat.chats.ChatsViewModel
import com.example.mychat.message.MessageActivity
import com.example.mychat.models.User
import de.hdodenhof.circleimageview.CircleImageView

class UsersAdapter(val context: Context, var users: List<User>) :
    RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    class UsersViewHolder(var context: Context, private val view: View) :
        RecyclerView.ViewHolder(view) {

        val username = itemView.findViewById<TextView>(R.id.username)
        val profileImage = itemView.findViewById<ImageView>(R.id.profile_image)
        val statusOnline = itemView.findViewById<CircleImageView>(R.id.img_on)
        val statusOffline = itemView.findViewById<CircleImageView>(R.id.img_off)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return UsersAdapter.UsersViewHolder(context, view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val user = users[position]
        holder.apply {
            username.text = user.username

            if (user.imageUrl == "default") {
                profileImage.setImageResource(R.mipmap.ic_launcher_round)
            } else {
                Glide.with(context).load(user.imageUrl).into(profileImage)
            }

            if (user.statusOnline) {
                statusOnline.visibility = View.VISIBLE
                statusOffline.visibility = View.GONE
            } else {
                statusOffline.visibility = View.VISIBLE
                statusOnline.visibility = View.GONE
            }

            username.setOnClickListener {
                startChatWithUser(user.id)
            }

            profileImage.setOnClickListener {
                startChatWithUser(user.id)
            }
        }
    }

    fun startChatWithUser(userId: String) {
        val intent = Intent(context, MessageActivity::class.java)
        intent.putExtra("userId", userId)
        context.startActivity(intent)
    }

    fun refreshUsers(users: ArrayList<User>) {
        this.users = users
        notifyDataSetChanged()
    }

}