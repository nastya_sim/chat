package com.example.mychat.users

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mychat.models.User
import com.example.mychat.models.UserModel

class UsersViewModel : ViewModel() {

    private val userModel = UserModel()
    val users = MutableLiveData<ArrayList<User>>()

    fun onCompleteLoad(users: ArrayList<User>){
        this.users.value=users
    }

    fun loadUsers(){
        userModel.getAllUsers(this::onCompleteLoad){}
    }

    fun searchUsers(str: String){
        userModel.searchUsers(str, this::onCompleteLoad){}
    }

}