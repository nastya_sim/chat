package com.example.mychat

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.mychat.auth.AuthViewModel
import com.example.mychat.auth.LoginActivity
import com.example.mychat.chats.ChatsFragment
import com.example.mychat.userprofile.UserProfileFragment
import com.example.mychat.users.UsersFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val authViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = ""

        val viewPagerAdapter=FragmentsAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(ChatsFragment(), "Chats")
        viewPagerAdapter.addFragment(UsersFragment(), "Users")
        viewPagerAdapter.addFragment(UserProfileFragment(), "Profile")
        authViewModel.getDataOfUser()
        authViewModel.user.observe(this, Observer {user->
            user?.let{
                username.text = user.username
                if(user.imageUrl == "default"){
                    profile_image.setImageResource(R.mipmap.ic_launcher_round)
                } else {
                    Glide.with(applicationContext).load(user.imageUrl).into(profile_image)
                }

            }
        })


        view_pager.adapter=viewPagerAdapter
        tab_layout.setupWithViewPager(view_pager)
        tab_layout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"))
        tab_layout.setSelectedTabIndicatorHeight((2 * resources.displayMetrics.density).toInt())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.logout -> {
                authViewModel.logout()
                startActivity(Intent(this, LoginActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
                finish()
                return true
            }
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        authViewModel.changeStatusOnline(true)
    }

    override fun onPause() {
        super.onPause()
        authViewModel.changeStatusOnline(false)
    }
}

