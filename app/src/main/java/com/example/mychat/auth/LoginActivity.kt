package com.example.mychat.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mychat.R
import com.example.mychat.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val authViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }

    override fun onStart() {
        super.onStart()
        authViewModel.currentUser?.let{
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = "Register"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn_enter.setOnClickListener {
            if (!email.text.isNullOrEmpty() and !password.text.isNullOrEmpty()) {
                progressBar.visibility= View.VISIBLE
                btn_register.isEnabled=false
                authViewModel.login(
                    email.text.toString(),
                    password.text.toString()
                )
                authViewModel.registerStatus.observe(this, Observer {
                    it?.let {
                        if (it) {
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        } else {
                            Toast.makeText(this, "repeat your attempt", Toast.LENGTH_SHORT).show()
                        }
                    }
                })
            }
        }

        btn_resert_password.setOnClickListener {
            startActivity(Intent(this, ResertPasswordActivity::class.java))
        }
    }
}
