package com.example.mychat.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mychat.R
import com.example.mychat.MainActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private val registerViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = "Register"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_register.setOnClickListener {
            if (!username.text.isNullOrEmpty() and !email.text.isNullOrEmpty() and !password.text.isNullOrEmpty()) {
                if(password.text!!.length>=6) {
                    progressBar.visibility = View.VISIBLE
                    btn_register.isEnabled = false
                    registerViewModel.register(
                        username.text.toString(),
                        email.text.toString(),
                        password.text.toString()
                    )
                    registerViewModel.registerStatus.observe(this, Observer {
                        if(it!=null) {
                            if (it) {
                                startActivity(Intent(this, MainActivity::class.java))
                            } else {
                                Toast.makeText(this, "repeat your attempt", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    })
                }else{
                    Toast.makeText(this, "Password must be more 6 chars", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

}
