package com.example.mychat.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mychat.models.UserModel
import com.example.mychat.models.User

class AuthViewModel: ViewModel() {

    private val userModel=UserModel()
    val registerStatus = MutableLiveData<Boolean>()
    val currentUser= userModel.getCurrentUser()
    val user=MutableLiveData<User>()
    val resultResetPasswordSendEmail = MutableLiveData<String>()

    private fun onCompleteLogin(userStatus: Boolean){
        registerStatus.value=userStatus
    }

    private fun onCompleteRegister(userStatus: Boolean){
        registerStatus.value=userStatus
    }

    private fun onCompleteDataOfUser(user: User){
        this.user.value=user
    }

    fun register(username: String, email: String, password: String){
        userModel.register(username, email, password, this::onCompleteRegister) { }
    }

    fun login(email: String, password: String){
        userModel.login(email, password, this::onCompleteLogin) { }
    }

    fun getDataOfUser(){
        userModel.getDataOfCurrentUser(this::onCompleteDataOfUser) { }
    }

    fun logout(){
        userModel.logout()
        registerStatus.value=false
    }

    fun changeStatusOnline(statusOnline: Boolean){
        userModel.changeStatusOnline(statusOnline)
    }

    private fun onCompleteSendEmail(resultResetPasswordSendEmail: String){
        this.resultResetPasswordSendEmail.value=resultResetPasswordSendEmail
    }

    fun sendPasswordResetEmail(email: String){
        userModel.sendPasswordResetEmail(email, this::onCompleteSendEmail){}
    }
}