package com.example.mychat.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mychat.R
import kotlinx.android.synthetic.main.activity_resert_password.*

class ResertPasswordActivity : AppCompatActivity() {

    private val authViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resert_password)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = "Register"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_reset.setOnClickListener{
            val email = send_email.text.toString()
            if(email.isNotEmpty()){
                authViewModel.sendPasswordResetEmail(email)
                authViewModel.resultResetPasswordSendEmail.observe(this, Observer {
                    it?.let {
                        if(it=="success"){
                            Toast.makeText(this, "Check your email", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, LoginActivity::class.java))
                        } else {
                            Toast.makeText(this, "Email doesn`t exist", Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } else {
                Toast.makeText(this, "Enter email", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
