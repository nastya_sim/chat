package com.example.mychat.message

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mychat.R
import com.example.mychat.models.Message

class MessageAdapter(val context: Context, var messages: List<Message>, val currentUserID: String) :
    RecyclerView.Adapter<MessageAdapter.ChatsViewHolder>() {

    val MSG_TYPE_LEFT = 0
    val MSG_TYPE_RIGHT = 1
    var imageUrl = String()

    class ChatsViewHolder(var context: Context, private val view: View) :
        RecyclerView.ViewHolder(view) {

        val showMessage = itemView.findViewById<TextView>(R.id.show_message)
        val profileImage = itemView.findViewById<ImageView>(R.id.profile_image)
        val txtSeen = itemView.findViewById<TextView>(R.id.text_seen)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatsViewHolder {
        if (viewType == MSG_TYPE_RIGHT) {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.chat_item_right, parent, false)
            return ChatsViewHolder(context, view)
        } else {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.chat_item_left, parent, false)
            return ChatsViewHolder(context, view)
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: ChatsViewHolder, position: Int) {
        val message = messages[position]
        holder.apply {
            showMessage.setText(message.message)

            if (getItemViewType(position) == MSG_TYPE_RIGHT) {
                if (position == messages.size - 1) {
                    txtSeen.visibility = View.VISIBLE
                    if (message.isseen=="true") {
                        txtSeen.text = "seen"
                    } else {
                        txtSeen.text = "delivered"
                    }
                } else {
                    txtSeen.visibility = View.GONE
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].sender == currentUserID) {
            MSG_TYPE_RIGHT
        } else {
            MSG_TYPE_LEFT
        }
    }

    fun refreshChats(Chats: ArrayList<Message>) {
        this.messages = Chats
        notifyDataSetChanged()
    }
}