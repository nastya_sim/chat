package com.example.mychat.message

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mychat.models.Message
import com.example.mychat.models.MessageModel
import com.example.mychat.models.User
import com.example.mychat.models.UserModel

class MessageViewModel : ViewModel(){
    private val userModel = UserModel()
    val user=MutableLiveData<User>()
    private val messageModel = MessageModel()
    val messages=MutableLiveData<ArrayList<Message>>()

    private fun onCompleteLoadDataOfUser(user: User){
        this.user.value=user
    }

    fun loadDataOfUser(userId: String){
        userModel.loadDataOfUser(userId, this::onCompleteLoadDataOfUser){}
    }

    fun sendMessage(sender: String, receiver: String, message: String) {
        messageModel.sendMessage(sender, receiver, message)
    }

    fun onCompleteLoadMessage(messages: ArrayList<Message>){
        this.messages.value=messages
    }

    fun loadMessages(currentUserId: String, userId: String){
        messageModel.loadMessages(currentUserId, userId, this::onCompleteLoadMessage){}
    }

    fun seenMessages(userId: String){
        messageModel.seenMessages(userId)
    }

    fun removeSeenListener(){
        messageModel.removeSeenListener()
    }

    fun sendNotification(receiver: String, username: String, message: String){
        //messageModel.sendNotification(receiver, username, message)
    }
}