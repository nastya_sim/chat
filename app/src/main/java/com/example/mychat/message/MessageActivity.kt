package com.example.mychat.message

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.mychat.MainActivity
import com.example.mychat.R
import com.example.mychat.auth.AuthViewModel
import com.example.mychat.models.Message
import com.example.mychat.notifications.APIService
import com.example.mychat.notifications.Client
import com.example.mychat.users.UsersViewModel
import kotlinx.android.synthetic.main.activity_message.*

class MessageActivity : AppCompatActivity() {

//    private val messageViewModel by lazy {
//        ViewModelProviders.of(this).get(MessageViewModel::class.java)
//    }
    private lateinit var messageViewModel: MessageViewModel
    private val authViewModel by lazy { ViewModelProviders.of(this).get(AuthViewModel::class.java) }
    lateinit var apiService: APIService
    //private var notify=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = " "
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            messageViewModel.removeSeenListener()
            finish()
            startActivity(Intent(this, MainActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }

        val userId = intent.getStringExtra("userId")

        apiService = Client().getClient("https://fcm.googlepis.com/").create(APIService::class.java)
        messageViewModel=MessageViewModel()
        messageViewModel.loadDataOfUser(userId)

        messageViewModel.user.observe(this, Observer {
            username.text = it.username
            if (it.imageUrl == "default") {
                profile_image.setImageResource(R.mipmap.ic_launcher_round)
            } else {
                Glide.with(applicationContext).load(it.imageUrl).into(profile_image)
            }
        })

        authViewModel.getDataOfUser()
        messageViewModel.seenMessages(userId)
        authViewModel.user.observe(this, Observer {sender ->
            btn_send.setOnClickListener {
                val message = text_send.text.toString()
                if (message.isNotEmpty()) {
                    //notify=true
                    //if(notify) {
                        sendMessage(sender.id, userId, message)
                     //   notify=false
                    //}
                    //messageViewModel.sendNotification(userId, "username", message)
                    text_send.setText("")
                } else {
                    Toast.makeText(this, "You can`t send empty message", Toast.LENGTH_SHORT).show()
                }
            }

            recycler_view_messages.setHasFixedSize(true)
            //recycler_view_messages.layoutManager= LinearLayoutManager(this)
            val linearLM=LinearLayoutManager(this)
            linearLM.stackFromEnd=true
            recycler_view_messages.layoutManager=linearLM
            val messages= arrayListOf<Message>()
            val messagesAdapter=MessageAdapter(this, messages, sender.id)
            recycler_view_messages.adapter=messagesAdapter

            messageViewModel.loadMessages(sender.id, userId)
            messageViewModel.messages.observe(this, Observer {
                //messageViewModel.seenMessages(userId)
                messagesAdapter.refreshChats(it)
                recycler_view_messages.layoutManager=linearLM
            })

        })

    }

    private fun sendMessage(sender: String, receiver: String, message: String) {
        messageViewModel.sendMessage(sender, receiver, message)
    }

    override fun onResume() {
        super.onResume()
        authViewModel.changeStatusOnline(true)
    }

    override fun onPause() {
        super.onPause()
        authViewModel.changeStatusOnline(false)
        messageViewModel.removeSeenListener()
    }

    override fun onBackPressed() {
        messageViewModel.removeSeenListener()
        finish()
        startActivity(Intent(this, MainActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }
}
