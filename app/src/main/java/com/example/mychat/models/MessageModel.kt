package com.example.mychat.models

import com.example.mychat.R
import com.example.mychat.notifications.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MessageModel {


    val firebaseDb = FirebaseDatabase.getInstance()
    val dbRefOnChat = firebaseDb.reference.child("Chats")
    val dbChatRef = firebaseDb.reference.child("Chats")
    val refOnMessages = FirebaseDatabase.getInstance().getReference("Chats")
    val refOnMessages2 = FirebaseDatabase.getInstance().getReference("Chats")
    private val auth = FirebaseAuth.getInstance()
    lateinit var seenListener: ValueEventListener

    fun sendMessage(sender: String, receiver: String, message: String) {
        val bodyMessage = hashMapOf<String, Any>()

        bodyMessage["sender"] = sender
        bodyMessage["receiver"] = receiver
        bodyMessage["message"] = message
        bodyMessage["isseen"] = "false"

        dbRefOnChat.push().setValue(bodyMessage)

//        val curUserId= auth.currentUser?.uid
//        val ref = firebaseDb.getReference("Users").child(curUserId!!)
//        ref.addValueEventListener(object : ValueEventListener{
//            override fun onCancelled(error: DatabaseError) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//            override fun onDataChange(snapshot: DataSnapshot) {
//                val user = snapshot.getValue(User::class.java)
//                if (user != null) {
//                    sendNotificationUser(receiver, user.username, message)
//                }
//
//            }
//
//        })


    }

    fun loadMessages(
        currentUserId: String, userId: String,
        onComplete: (messages: ArrayList<Message>) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val messages = arrayListOf<Message>()
        refOnMessages.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                messages.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                    val message = snapshot.getValue(Message::class.java)
                    message?.let {
                        if (((it.receiver == currentUserId) and (it.sender == userId)) or
                            ((it.sender == currentUserId) and (it.receiver == userId))
                        ) {
//                            if ((message.receiver == currentUserId) and (message.isseen=="false")) {
//                                val mapOfMessageSeen = hashMapOf<String, Any>()
//                                mapOfMessageSeen["isseen"] = "true"
//                                snapshot.ref.updateChildren(mapOfMessageSeen)
//                                it.isseen = "true"
//                            }

                            messages.add(it)
                        }
                    }
                }
                onComplete(messages)
            }

        })
    }

    fun seenMessages(userId: String) {
        val messages = arrayListOf<Message>()
        val currentUserId = auth.currentUser?.uid

        seenListener = refOnMessages2.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                messages.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                    val message = snapshot.getValue(Message::class.java)
                    message?.let {
                        if ((message.receiver == currentUserId) and (it.sender == userId) and (message.isseen == "false")) {
                            val mapOfMessageSeen = hashMapOf<String, Any>()
                            mapOfMessageSeen["isseen"] = "true"
                            snapshot.ref.updateChildren(mapOfMessageSeen)
                        }
                    }
                }
            }

        })
    }


    fun loadUsersChats(
        onComplete: (users: ArrayList<User>) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val currentUserId = auth.currentUser?.uid
        currentUserId?.let {
            val usersId = arrayListOf<String>()
            var usersWithChats = arrayListOf<User>()
            refOnMessages.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    onFailed(Exception())
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    usersId.clear()
                    for (snapshot: DataSnapshot in dataSnapshot.children) {
                        val message = snapshot.getValue(Message::class.java)
                        message?.let {
                            //var pairUserLastMessage=Pair<String, String>(message.sender, message.message)
                            if ((it.receiver == currentUserId) and (!usersId.contains(it.sender))) {
                                usersId.add(it.sender)
                            }
                            if ((it.sender == currentUserId) and (!usersId.contains(it.receiver))) {
                                usersId.add(it.receiver)
                            }
                        }
                    }
                    val userModel = UserModel()
                    userModel.loadUsers(usersId, {
                        usersWithChats = it
                        onComplete(usersWithChats)
                    }, {
                        onFailed(Exception())
                    })
                }
            })
        }
    }

    fun loadLastChatMessage(
        user: User,
        onComplete: (lastMessagge: Chat) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val currentUserId = auth.currentUser?.uid

        var lastMessage = Triple(String(), String(), String() )

        refOnMessages.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                     val message = snapshot.getValue(Message::class.java)
                    message?.let {
                        if ((it.receiver == currentUserId) and (it.sender == user.id)) {
                            lastMessage = Triple(currentUserId!!, it.message, it.isseen)
                        } else if ((it.sender == currentUserId) and (it.receiver == user.id)) {
                            lastMessage = Triple(user.id, it.message, it.isseen)
                        }

                    }
                }
                val triple = Chat(user, lastMessage.first, lastMessage.second, lastMessage.third)
                onComplete(triple)
            }
        })

    }

//fun seenMessage(userId: String) {
//    val currentUserId = auth.currentUser?.uid
//
//    seenListener = dbRefOnChat.addValueEventListener(object : ValueEventListener {
//        override fun onCancelled(error: DatabaseError) {
//            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//        }
//
//        override fun onDataChange(dataSnapshot: DataSnapshot) {
//            for (snapshot: DataSnapshot in dataSnapshot.children) {
//                val message = snapshot.getValue(Message::class.java)
//                message?.let {
//                    if ((message.receiver == currentUserId) and (message.sender == userId)) {
//                        val mapOfMessageSeen = hashMapOf<String, Any>()
//                        mapOfMessageSeen["isseen"] = true
//                        snapshot.ref.updateChildren(mapOfMessageSeen)
//                    }
//                }
//            }
//        }
//    })
//}

    fun removeSeenListener() {
        //dbRefOnChat.removeEventListener(seenListener)
        refOnMessages2.removeEventListener(seenListener)
    }

    fun sendNotification(receiver: String, username: String, message: String){
        if(auth.currentUser!=null) {
            val refOnUser = firebaseDb.getReference("Users").child(auth.currentUser!!.uid)
            refOnUser.addValueEventListener(object : ValueEventListener{
                override fun onCancelled(error: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val user=snapshot.getValue(User::class.java)
                    if (user != null) {
                        sendNotificationUser(receiver, user.username, message)
                    }
                }
            })

        }
    }

    fun sendNotificationUser(receiver: String, username: String, message: String){
        val tokens = FirebaseDatabase.getInstance().getReference("Tokens")
        val query = tokens.orderByKey().equalTo(receiver)
        query.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for(snapshot: DataSnapshot in dataSnapshot.children){
                    val token = snapshot.getValue(Token::class.java)
                    val data = Data(
                        auth.currentUser!!.uid, R.mipmap.ic_launcher_round,
                        "$username: $message", "New message", receiver)

                    val sender = token?.token?.let { Sender(data, it) }

                    val apiService= Client().getClient("https://fcm.googleapis.com/").create(
                        APIService::class.java)

                    if (sender != null) {
                        apiService.sendNotification(sender)
                            .enqueue(object : Callback<NotificationResponse> {
                                override fun onFailure(
                                    call: Call<NotificationResponse>,
                                    t: Throwable
                                ) {
                                    val a=1
                                }

                                override fun onResponse(
                                    call: Call<NotificationResponse>,
                                    response: Response<NotificationResponse>
                                ) {
                                    if(response.code() == 200){
                                        if(response.body()?.succes != 1){
                                            //false
                                            val a=1


                                        }
                                    }
                                }

                            })
                    } else {
                        val a=1
                    }
                }
            }
        })
    }

}