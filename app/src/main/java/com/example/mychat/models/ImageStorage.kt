package com.example.mychat.models

import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask

class ImageStorage(val context: Context) {

    private val storageReference = FirebaseStorage.getInstance().getReference("uploads")
    private val currentUserId= FirebaseAuth.getInstance().currentUser?.uid

        private fun getFileExtension(uri: Uri): String? { //опр тип файла
        val counterResolver = context.contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(counterResolver.getType(uri))
    }

    fun uploadImage(
        imageUri: Uri,
        onComplete: (isSuccess: Boolean) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val fileReference = storageReference.child(
            System.currentTimeMillis().toString() + "." + getFileExtension(imageUri)
        )

        val uploadTask = fileReference.putFile(imageUri)

        uploadTask.continueWithTask(object : Continuation<UploadTask.TaskSnapshot, Task<Uri>>{
            override fun then(p0: Task<UploadTask.TaskSnapshot>): Task<Uri> {
                if (!p0.isSuccessful) {
                    throw p0.exception!!
                }
                return fileReference.downloadUrl
            }

        } ).addOnCompleteListener {
            if (it.isSuccessful) {
                val downloadUri = it.result
                downloadUri?.let {
                    val uri = it.toString()
                    currentUserId?.let {
                        val refOnUser = FirebaseDatabase.getInstance().getReference("Users")
                            .child(currentUserId)
                        val map = hashMapOf<String, String>()
                        map.put("imageUrl", uri)
                        refOnUser.updateChildren(map as Map<String, Any>)
                        onComplete(true)
                    }
                }
            } else {
                //onComplete(false)
            }
        }.addOnFailureListener {
            onFailed(Exception())
        }
    }
}