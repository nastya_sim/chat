package com.example.mychat.models

class Chat {
    var user = User()
        get() = field
    var sender = String()
        get() = field
    var message = String()
        get() = field
    var messageIsSeen = String()
        get() = field

    constructor()

    constructor(user: User, sender: String, message: String, messageIsSeen: String) {
        this.user = user
        this.sender=sender
        this.message=message
        this.messageIsSeen= messageIsSeen
    }
}