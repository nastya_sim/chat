package com.example.mychat.models

class User {
    var id = String()
        get() = field
    var username = String()
        get() = field
    var search = String()
        get() = field
    var imageUrl = String()
        get() = field
    var statusOnline = true
        get() = field

    constructor()

    constructor(id: String, username: String, imageUrl: String, status: Boolean) {
        this.id = id
        this.username = username
        this.imageUrl = imageUrl
        statusOnline=status
        search=username.toLowerCase()
    }
}