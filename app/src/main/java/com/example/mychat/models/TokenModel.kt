package com.example.mychat.models

import com.example.mychat.notifications.Token
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class TokenModel {
    val refOnTokens=FirebaseDatabase.getInstance().getReference("Tokens")
    val curUser=FirebaseAuth.getInstance().currentUser

    fun updateToken(token: String){
        val newToken= Token(token)
        if (curUser != null) {
            refOnTokens.child(curUser.uid).setValue(newToken)
        }
    }
}