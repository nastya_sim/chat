package com.example.mychat.models

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class UserModel {

    val auth = FirebaseAuth.getInstance()
        get() = field
    private val dbRefOnUsers = FirebaseDatabase.getInstance().getReference("Users")

    fun getCurrentUser(): FirebaseUser? {
        auth.currentUser?.let {
            return it
        }
        return null
    }

    fun login(
        email: String, password: String,
        onComplete: (isRegister: Boolean) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    //ассинхр
                    onComplete(true)
                } else {
                    onComplete(false)
                }

            }
            .addOnFailureListener {
                onFailed(Exception())
            }
    }


    fun register(
        username: String, email: String, password: String,
        onComplete: (isRegister: Boolean) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val currentUser = auth.currentUser
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener() {
                if (it.isSuccessful) {
                    val user = auth.currentUser
                    val userId = user!!.uid
                    val ref = dbRefOnUsers.child(userId)

                    val dataOfUser = HashMap<String, Any>()
                    dataOfUser["id"] = userId
                    dataOfUser["username"] = username
                    dataOfUser["imageUrl"] = "default"
                    dataOfUser["statusOnline"] = false
                    dataOfUser["search"] = username.toLowerCase()

                    ref.setValue(dataOfUser).addOnCompleteListener {
                        //ассинхр
                        onComplete(true)
                    }.addOnFailureListener {
                        onFailed(Exception())
                    }
                } else {
                    onComplete(false)
                }
            }
            .addOnFailureListener {
                onFailed(Exception())
            }
    }

    fun getDataOfCurrentUser(
        onComplete: (user: User) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val firebaseUser = getCurrentUser()
        firebaseUser?.let { user ->
            val ref = dbRefOnUsers.child(user.uid)

            ref.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    onFailed(Exception())
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val user = dataSnapshot.getValue(User::class.java)
                    if (user != null) {
                        onComplete(user)
                    }
                }
            })
        }

    }

    fun logout() {
        auth.signOut()
    }

    fun getAllUsers(
        onComplete: (users: ArrayList<User>) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val users = arrayListOf<User>()
        val firebaseUser = auth.currentUser

        dbRefOnUsers.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                users.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                    val user = snapshot.getValue(User::class.java)
                    user?.let {
                        if (user.id != firebaseUser?.uid)
                            users.add(user)
                    }
                }
                onComplete(users)
            }
        })
    }

    fun loadDataOfUser(
        userId: String,
        onComplete: (user: User) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val ref = dbRefOnUsers.child(userId)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                user?.let {
                    onComplete(user)
                }
            }
        })
    }

    fun loadUsers(
        usersId: ArrayList<String>, onComplete: (users: ArrayList<User>) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val users = ArrayList<User>()
        dbRefOnUsers.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                users.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                    val user = snapshot.getValue(User::class.java)
                    user?.let {
                        if (usersId.contains(user.id)) {
                            users.add(user)
                        }
                    }
                }
                onComplete(users)
            }
        })
    }

    fun loadDataOfCurrentUser(
        onComplete: (user: User) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val currentUser = getCurrentUser()
        currentUser?.let { user ->
            val refOnUser = dbRefOnUsers.child(user.uid)

            refOnUser.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    onFailed(Exception())
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val dataOfUser = snapshot.getValue(User::class.java)
                    dataOfUser?.let {
                        onComplete(it)
                    }
                }

            })
        }
    }

    fun changeStatusOnline(statusOnline: Boolean) {
        getCurrentUser()?.uid?.let {
            val refOnUserForId = dbRefOnUsers.child(it)
            val statusUser = hashMapOf<String, Any>()
            statusUser["statusOnline"] = statusOnline

            refOnUserForId.updateChildren(statusUser)
        }
    }

    fun searchUsers(
        str: String,
        onComplete: (users: ArrayList<User>) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        val fbUser = getCurrentUser()
        val users = ArrayList<User>()

//        val query = dbRefOnUsers.orderByChild("search")
//            .startAt(str.toLowerCase())
//            .endAt(str.toLowerCase()+"\uf8ff")
        val query = dbRefOnUsers.orderByChild("search")

        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                onFailed(Exception())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                users.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children) {
                    val user = snapshot.getValue(User::class.java)
                    user?.let {
                        if ((user.id != fbUser?.uid) and (user.search.contains(str.toLowerCase()))) {
                            users.add(user)
                        }
                    }
                }
                onComplete(users)
            }
        })
    }

    fun sendPasswordResetEmail(
        email: String,
        onComplete: (taskResult: String) -> Unit,
        onFailed: (exception: Exception) -> Unit
    ) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                onComplete("success")
            } else task.exception?.message?.let { onComplete(it) }
        }.addOnFailureListener {
            onFailed(Exception())
        }
    }

}