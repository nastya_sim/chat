package com.example.mychat.models

class Message {
    var sender = String()
        get() = field
    var receiver = String()
        get() = field
    var message = String()
        get() = field
    var isseen="false"
        get() = field

    constructor()

    constructor(receiver: String, sender:String, message: String){
        this.sender=sender
        this.receiver=receiver
        this.message=message
    }

}

//data class Message(val sender: String, val receiver: String, val message: String)